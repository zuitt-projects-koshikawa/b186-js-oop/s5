class Customer {
  constructor(email) {
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }
  checkOut() {
    if (this.cart.contents.length > 0) {
      this.orders.push({
        products: this.cart.contents,
        totalAmount: this.cart.computeTotal().totalAmount,
      });
    }
    return this;
  }
}

class Cart {
  constructor() {
    this.contents = [];
    this.totalAmount = 0;
  }

  addToCart(product, quantity) {
    this.contents.push({ product: product, quantity: quantity });
    return this;
  }

  showCartContents() {
    return this.contents;
  }

  updateProductQuantity(name, quantity) {
    this.contents.find((element) => element.product.name === name).quantity =
      quantity;
    return this;
  }

  clearCartContent() {
    this.contents = [];
    return this;
  }

  computeTotal() {
    this.totalAmount = 0;
    if (this.contents.length > 0) {
      this.contents.forEach((element) => {
        this.totalAmount += element.product.price * element.quantity;
      });
    }
    return this;
  }
}

class Product {
  constructor(name, price) {
    this.name = name;
    if (typeof price === 'number') {
      this.price = price;
    } else {
      console.log(`${this.price} is not a number`);
      this.price = 'undefined';
    }
    this.isActive = true;
  }

  archive() {
    this.isActive = false;
    return this;
  }

  updatePrice(newPrice) {
    this.price = newPrice;
    return this;
  }
}

const john = new Customer('john@mail.com');
const prodA = new Product('soap', 9.99);
const prodB = new Product('shampoo', 10);

john.cart.addToCart(prodA, 3);
john.cart.addToCart(prodB, 5);
